
-- Funcion que regresa el n-esimo elemento de una lista
myNthElement :: Int -> [a] ->  a
myNthElement n [] = error "Invalid index."
myNthElement 0 (x:xs) = x
myNthElement n (x:xs) = myNthElement (n-1) xs

-- Definimos los comandos.
data Command = I Int
	| ADD
	| DIV
	| Eq
	| EXEC
	| Gt
	| Lt
	| MUL
	| NGET
	| POP
	| REM
	| SEL
	| SUB
	| SWAP
	| ES [Command] deriving (Show, Eq)

-- Definimos la palabra reservada postfix.
data PF = POSTFIX deriving (Show, Eq)

-- Definimos la representacion de los programas.
type Program = (PF, Int, [Command])

-- Definimos la pila de valores.
type Stack = [Command]

-- Funcion que realiza las operaciones de los comandos 
-- aritmmeticos:
-- (add, div, eq, gt, lt, mul, rem, sub).
arithOperation :: Command -> Command -> Command -> Command
arithOperation (I v) (I w) SUB = (I (w - v))
arithOperation (I v) (I w) ADD = (I (w + v))
arithOperation (I v) (I w) MUL = (I (w * v))
arithOperation (I v) (I w) DIV = (I (div w v))
arithOperation (I v) (I w) REM = (I (mod w v))
arithOperation (I v) (I w) Lt = if w < v
	then (I 1)
	else (I 0)
arithOperation (I v) (I w) Gt = if w > v
	then (I 1)
	else (I 0)
arithOperation (I v) (I w) Eq = if w == v
	then (I 1)
	else (I 0)
arithOperation _ _ _ = error "Command1 and Command2 must be a integer number."


-- Funcion que realiza las operaciones de los comandos que 
-- alteran la pila de valores:
-- (literal entera, nget, pop, sel, swap, secuencia ejecutable).
stackOperation :: Stack -> Command -> Stack
stackOperation xs (I n) = (I n):xs
stackOperation ((I n):xs) NGET = (myNthElement n ((I n):xs)):xs 
stackOperation (x:(y:((I n):zs))) SEL = if n == 0
	then x:zs
	else y:zs
stackOperation (x:xs) POP = xs
stackOperation (x:(y:ys)) SWAP = y:(x:ys)
stackOperation xs (ES commandList) = (ES commandList): xs
stackOperation _ _ = error "Invalid input data."

-- Funcion que devuelve la lista de comandos y la pila 
-- resultante de realizar la llamada a la operacion exec:
execOperation :: [Command] -> Stack -> ([Command], Stack)
execOperation cs ((ES x):s) = (x ++ cs, s)
execOperation _ _ = error "Invalid stack."

-- Funcion que dada una lista de comandos y una pila de valores 
-- obtiene la pila de valores resultante despues ejecutar 
-- todos los comandos:
executeCommand :: [Command] -> Stack -> Stack
executeCommand [] xs = xs
executeCommand (EXEC:cs) ((ES x):xs) = executeCommand (x++cs) xs
executeCommand (arithCommand:cs) ((I n):((I m):xs)) = 
	executeCommand cs ((arithOperation (I n) (I m) arithCommand):xs)
executeCommand (stackCommand:cs) xs = 
	executeCommand cs (stackOperation xs stackCommand)

-- Funcion que determina si la pila de valores que se desea 
-- ejecutar con un programa es valida:
validProgram :: Program -> Stack -> Bool
validProgram (_, 0, _) xs = if xs == [] 
	then True
	else False
validProgram (pf, n, cs) ((I m):xs) = validProgram (pf, (n - 1), cs) xs
validProgram (_, n, _) (_:xs) = False 

-- Funcion que ejecuta cualquier programa en Postfix.
executeProgram :: Program -> Stack -> Command
executeProgram (POSTFIX, n, cs) stack = if validProgram (POSTFIX, n, cs) stack
	then let result = executeCommand cs stack
		in myNthElement 0 result
	else error "Invalid stack."