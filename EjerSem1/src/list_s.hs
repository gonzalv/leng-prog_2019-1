data ListS a = NilS | Snoc (ListS a) a deriving Show

-- Obtiene el primer elemento de la lista.
headS :: ListS a -> a
headS NilS = error "Empty List."
headS (Snoc NilS a) = a
headS (Snoc xs a) = headS xs

-- Obtiene la lista sin el primer elemento.
tailS :: ListS a -> ListS a
tailS NilS = error "Empty List."
tailS (Snoc NilS a) = NilS
tailS (Snoc xs a) = Snoc (tailS xs) a

-- Obtiene la lista sin el ultimo elemento.
initS :: ListS a -> ListS a
initS NilS = error "Empty List."
initS (Snoc xs a) = xs 

-- Obtiene el ultimo elemento de la lista.
lastS :: ListS a -> a
lastS NilS = error "Empty List."
lastS (Snoc xs a) = a

-- Obtiene el n-esimo elemento de la lista.
nthElementS :: Int -> ListS a -> a
nthElementS 0 xs = headS xs
nthElementS n NilS = error "Invalid index." 
nthElementS n xs = if n < 0 
	then error "Invalid index." 
	else nthElementS (n - 1) (tailS xs) 

-- Obtiene la lista donde el primer elemento es el elemento dado.
addFirstS :: a -> ListS a -> ListS a
addFirstS elem NilS = Snoc NilS elem
addFirstS elem (Snoc xs a) = Snoc (addFirstS elem xs) a

-- Elimina el n-esimo elemento.
deleteNthElementS :: Int -> ListS a -> ListS a
deleteNthElementS n NilS = NilS
deleteNthElementS 0 xs = tailS xs
deleteNthElementS n xs = if n < 0 
	then error "Invalid index."
	else addFirstS (headS xs) (deleteNthElementS (n-1) (tailS xs))  

-- Obtiene la lista donde el ultimo elemento es el elemento dado.
addLastS :: a -> ListS a -> ListS a
addLastS elem xs = Snoc xs elem

-- Obtiene la reversa de la lista.
reverseS :: ListS a -> ListS a
reverseS NilS = NilS
reverseS (Snoc xs a) = addFirstS a (reverseS xs)

-- Obtiene la concatenacion de dos listas.
appendS :: ListS a -> ListS a -> ListS a
appendS NilS ys = ys
appendS (Snoc xs a) ys = appendS xs (addFirstS a ys)

-- Obtiene la lista con los primeros n elementos.
takeS :: Int -> ListS a -> ListS a
takeS n NilS = NilS
takeS 0 xs = NilS
takeS n xs = addFirstS (headS xs) (takeS (n-1) (tailS xs)) 