data Nat = Zero | D Nat | O Nat deriving Show

-- Obtiene la representación en numeros Nat de un numero entero.
toNat :: Int -> Nat
toNat 0 = Zero
toNat 1 = O Zero
toNat n = if (mod n 2) == 0
	then D (toNat (div n 2))
	else O (toNat (div (n - 1) 2))

-- Obtiene el sucesor de un numero Nat.
mySucc :: Nat -> Nat
mySucc Zero = O Zero
mySucc (D nat) = O nat
mySucc (O nat) = D (mySucc nat)

-- Obtiene el predecesor del numero Nat.
myPred :: Nat -> Nat 
myPred Zero = error "Zero has no predecessor."
myPred (O Zero) = Zero
myPred (O nat) = D nat
myPred (D nat) = O (myPred nat)

-- Obtiene la suma de dos numeros Nat.
myAdd :: Nat -> Nat -> Nat
myAdd Zero nat = nat
myAdd natX natY = myAdd (myPred natX) (mySucc natY)

-- Obtiene el producto de dos numeros Nat.
myProd :: Nat -> Nat -> Nat
myProd Zero nat = Zero
myProd natX natY = myAdd natY (myProd (myPred natX) natY)
